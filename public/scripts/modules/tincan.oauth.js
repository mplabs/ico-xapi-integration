(function (hello) {
  'use strict';

  hello.init({

    tincan: {

      name: 'tincan',

      oauth: {
        version: '2',
        response_type: 'code',
        auth: 'https://tincan.mplabs.de/xAPI/OAuth/authorize',
        grant: 'https://tincan.mplabs.de/xAPI/OAuth/access_token'
      },

      scope: {
        basic: ''
      },

      base: 'https://tincan.mplabs.de/xAPI/',

    }

  });
}(hello));