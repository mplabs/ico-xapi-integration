/**
 * A plugin for the ADL xAPI Wrapper to enable a common
 * workflow of registring and executing an interactive 
 * content object to and from an e-learning platform
 * 
 * @author Felix Dürrwald <mplabs@mplabs.de>
 * @since 2015-05-07
 * @version 0.0.2
 * @link https://bitbucket.org/mplabs
 * @link https://github.com/adlnet/xAPIWrapper
 */
 (function Integration (ADL) {
   'use strict';

   // Check dependencies
  if ( "undefined" === typeof ADL.XAPIWrapper ||
       "undefined" === typeof ADL.XAPIWrapper.xapiVersion || 
       "1.0.1" !== ADL.XAPIWrapper.xapiVersion ) {
    throw "ADL XAPIWrapper 1.0.1 must be loaded. Visit https://github.com/adlnet/xAPIWrapper.";
    return;
  }

  /**
   * Utilities
   */

  /**
   * Integration Class
   */
  function Integration () {

    /**
     * Private
     */

    /**
     * Reference to the current instance
     */
    var self = this;

    /**
     * @var {object} value-map containing the browsers query parameters
     * @private
     */
    var queryParams = (function ( qs ) {
      var params = {};
      var re = /[?&]?([^=]+)=([^&]*)/g;
      var tokens;

      qs = qs.replace(/\+/g, " ");
   
      while ( tokens = re.exec(qs) ) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
      }
   
      return params;
    })(document.location.search || "");

    /**
     * AJAX get document from url
     *
     * @param {string} url
     * @return Promise
     */
    function _get ( url ) {

       return new Promise( function (resolve, reject) {

        var xmlhttp = new XMLHttpRequest;
        xmlhttp.open("GET", url);
        
        xmlhttp.onreadystatechange = function () {

          if ( xmlhttp.readyState === 4 && xmlhttp.status === 200 ) {
            
            resolve( xmlhttp.responseText );

          } else if ( xmlhttp.readyState === 4 && xmlhttp.status !== 200 ) {

            reject( Error("Could not load manifest") );

          }

        };

        xmlhttp.send();

      });

    };

    /**
     * Parse a JSON string
     *
     * @param {string} data
     * @return object
     */
    function _parseJSON ( data ) {

      try {
        return JSON.parse( data );
      } catch ( e ) {
        throw "Malformed JSON";
      }

    };

    function _setup ( conf ) {
      for (var k in conf) {
        if (k in self) {
          self[k] = conf[k];
        }
      }
    };

    /**
     * Public
     */

    this.setActivityId = function ( activityId ) {
      this.activityId = activityId;

      return this;
    };

    this.setProfileId = function ( profileId ) {
      this.profileId = profileId;

      return this;
    };

    this.setEndpoint = function ( endpoint ) {
      this.endpoint = endpoint;

      ADL.XAPIWrapper.changeConfig({
        endpoint: endpoint
      });

      return this;
    };

    this.setAuth = function ( auth ) {
      this.auth = auth;

      ADL.XAPIWrapper.changeConfig({
        auth: auth
      });

      return this;
    };

    /**
     * Get the activity profile
     *
     * @param {string} profileId (optional)
     * @return {Promise}
     */
    this.getActivityProfile = function ( profileId ) {

      profileId = profileId || self.profileId;

      if ( ! profileId ) throw Error("ProfileId not set.");

      return new Promise( function ( resolve, reject ) {

        ADL.XAPIWrapper
          .getActivityProfile(
            self.activityId,
            profileId,
            null, // since
            function ( XMLHttpResponse ) {
              resolve(
                _parseJSON(XMLHttpResponse.responseText)
              );
            }
          );

      });

    };

    /**
     * Get values from the query string
     *
     * @param {string} key The key to get (optional)
     * @return {mixed} object, or value, or NULL
     */
    this.getFromQueryString = function ( key ) {

      if ( undefined === key ) {
        return queryParams;
      }

      return queryParams[key] || null;

    };

    /**
     * Setup the Integration
     * 
     * @param  {object} configuration
     * @return Promise
     */
    this.setup = function ( conf ) {
      
      return new Promise( function ( resolve, reject ) {

        _setup(conf);
        resolve( self );

      });

    };

    /**
     * Get the local manifest
     *
     * @param {string} url (optional)
     * @return Promise
     */
    this.setupFromManifest = function ( url ) {

      url = url || 'manifest.json';

      return new Promise( function ( resolve, reject ) {

        _get( url )
          .then(_parseJSON)
          .then(_setup)
          .then(function () {
            resolve( self );

          });

      });

    };

    /**
     * Get the query parameters
     *
     * @return Promise
     */
    this.setupFromQueryParameters = function () {

      return new Promise( function ( resolve, reject ) {

        self.setup( self.getFromQueryString() )
          .then(function () {
            resolve( self );

          });

      });

    };

    /**
     * return an ISO 8601 duration string
     * @param  {int} duration [duration in microseconds]
     * @return {string}
     */
    this.getISO8601Duration = function (duration) {
      var result = 'P';
      result += (duration / 31536000000 > 1) ? (duration / 31536000000 ).toFixed().toString() + 'Y' : '';
      result += (duration / 2628000000 > 1) ? (duration / 2628000000).toFixed().toString() + 'M' : '';
      result += (duration / 606461538.4615385 > 1) ? (duration / 606461538.4615385).toFixed().toString() + 'W' : '';
      result += (duration / 86400000 > 1) ? (duration / 86400000).toFixed().toString() + 'D' : '';
      result += 'T';
      result += (duration / 3600000 > 1) ? (duration / 3600000).toFixed().toString() + 'H' : '';
      result += (duration / 60000 > 1) ? (duration / 60000).toFixed().toString() + 'M' : '';
      result += (duration / 1000).toString() + 'S';

      return result;
    };

  };

  Integration.prototype = {

    activityId: null,

    profileId: null,

    endpoint: null,

    auth: null,

  };

  ADL.Integration = new Integration;

 }(window.ADL = window.ADL || {}));