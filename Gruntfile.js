module.exports = function(grunt) {
    'use strict';

    // measures the time each task takes
    require('time-grunt')(grunt);

    // Load the plugin that provides tasks.
    require('load-grunt-tasks')(grunt);

    // Load all custom tasks
    grunt.loadTasks('tasks');

    // Load grunt configurations
    var options = {
        config: { // set default configs location
            src: 'tasks/configs/*.js'
        },
        pkg: grunt.file.readJSON('package.json'),
        param: { // Project settings
            src: 'public',
            build: 'build',
            tmp: '.tmp',
            dst: 'dist',
            pack: 'pack',
            www: 'www'
        }
    };
    var configs = require('load-grunt-configs')(grunt, options);
    // Define the configuration for all the tasks
    grunt.initConfig(configs);

    // intemediate task to optimize resources
    grunt.registerTask('optimize', [
        'copy:build',
        'useminPrepare',
        'concat:generated',
        'cssmin:generated',
        'uglify:generated',
        'htmlmin',
        'usemin'
    ]);

    // generate package app to pack/
    grunt.registerTask('pack', [
        'welcome',
        'clean:dist',
        'copy:static',
        'concat:pack',
        'clean:pack',
        'compress:pack', // Als tar packen
        'fileExists:pack' // test result
    ]);

    // generate package app to pack/

    // generate package app to pack/
    grunt.registerTask('deployLocal', [
        'pack',
        'exec:deployLocal'
    ]);
    // generate package app to pack/
    grunt.registerTask('deployIntegration', [
        'pack',
        'exec:deployIntegration'
    ]);

    // lint
    grunt.registerTask('lint', [
        'welcome', 'jshint', 'jscs', 'jsonlint', 'csslint', 'sloc'
    ]);

    // Default server test task.
    grunt.registerTask('default', [
        'test'
    ]);

    // Default client test task.
    grunt.registerTask('test', [
        'lint', 'karma'
    ]);

    // generate docs
    grunt.registerTask('docs', [
        'clean:docs', 'lint', 'jsdoc'
    ]);

    // generate id
    grunt.registerTask('generateId', [
        'file-creator:id'
    ]);

    grunt.registerTask('updateWebappManifest', [
        'package-param', 'json-replace:manifest.webapp'
    ]);
    grunt.registerTask('newReleaseNumber', [
        'package-param',
        'prompt:release',
        'replace',
        'json-replace:manifest.webpackage',
        'package-version',
        'json-replace:package.json'
    ]);

};

